package com.example.application3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText

private lateinit var etName:EditText
private lateinit var etSurname:EditText
private lateinit var etFName:EditText
private lateinit var etAge:EditText
private lateinit var etHobby:EditText
private lateinit var btNext:Button
private lateinit var cBcheck:CheckBox

private var sList=""
private const val TAG="MainActivity"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        etName=findViewById(R.id.etName)
        etSurname=findViewById(R.id.etSurname)
        etFName=findViewById(R.id.etFName)
        etAge=findViewById(R.id.etAge)
        etHobby=findViewById(R.id.etHobby)
        btNext=findViewById(R.id.btNext)
        cBcheck=findViewById(R.id.cBcheck)

        val intent=Intent(this,InfoActivity::class.java)

        etName.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"nameChanged $p0")
            }

        })

        etSurname.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"surnameChanged $p0")
            }

        })

        etFName.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"fNameChanged $p0")
            }

        })

        etAge.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"ageChanged $p0")
            }

        })

        etHobby.addTextChangedListener(object:TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                Log.i(TAG,"hobbyChanged $p0")
            }

        })

        btNext.setOnClickListener(object: View.OnClickListener{
            override fun onClick(p0: View?) {
                if (cBcheck.isChecked) {
                    sList+= etName.text.toString()+'\n'+etSurname.text.toString()+'\n'+etFName.text.toString()+'\n'+ etHobby.text.toString()
                    intent.putExtra(Constants.id, sList)
                    intent.putExtra(Constants.idFI, etAge.text.toString().toInt())
                    startActivity(intent)
                }
            }

        })
    }
}